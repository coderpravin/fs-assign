import React from 'react';
import { Redirect, Link } from 'react-router-dom';
import { Button } from 'semantic-ui-react';
import TableExampleStriped from '../Table/index';

class Dashboard extends React.Component {
  isAuthenticated() {
    const token = sessionStorage.getItem('token');
    if (token) {
      return true;
    } else {
      return false;
    }
  }
  render() {
    const isUserAuthenticated = this.isAuthenticated();
    return (
      <div>
        {!isUserAuthenticated ? (
          <Redirect to={{ pathname: '/' }} />
        ) : (
          <div>
            <div className="hm-dshbd-ttl">
              {`Hi, Welcome to your dashboard`}{' '}
              <Link to="/adduser">
                <Button primary style={{ float: 'right' }}>
                  Add User
                </Button>
              </Link>
            </div>
            <TableExampleStriped />
          </div>
        )}
      </div>
    );
  }
}

export default Dashboard;
