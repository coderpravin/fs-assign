import React from 'react';
import { Button, Container, Form } from 'semantic-ui-react';
import axios from 'axios';


class AddUser extends React.Component {
  constructor(props) {
    super();
    this.state = {
      firstName: '',
      lastName: '',
      dob: '',
      email: '',
      mobile: '',
      gender: ''
    };
  }

  addUser(newUser) {
    axios
      .post('https://5b2de73b47942a001493691b.mockapi.io/users', newUser)
      .then(response => {
        console.log(response.data);
        alert('You successfully added a user');
        this.props.history.push('/dashboard');
      })
      .catch(error => {
        console.log(error);
      });
  }
  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onSubmit(e) {
    e.preventDefault();
    this.addUser(this.state);
  }

  render() {
    return (
      <Container>
        <h1>Add user to the list</h1>
        <Form onSubmit={this.onSubmit.bind(this)}>
          <Form.Field>
            <label>First Name</label>
            <input
              type="text"
              name="firstName"
              placeholder="First Name"
              onChange={this.handleChange.bind(this)}
            />
          </Form.Field>
          <Form.Field>
            <label>Last Name</label>
            <input
              type="text"
              name="lastName"
              placeholder="Last Name"
              onChange={this.handleChange.bind(this)}
            />
          </Form.Field>
          <Form.Field>
            <label>Email</label>
            <input
              type="email"
              name="email"
              placeholder="Email"
              onChange={this.handleChange.bind(this)}
            />
          </Form.Field>
          <Form.Field>
            <label>Mobile No.</label>
            <input
              type="text"
              name="mobile"
              placeholder="Mobile No."
              onChange={this.handleChange.bind(this)}
            />
          </Form.Field>
          <Form.Field>
            <label>Birth Year</label>
            <input
              type="number"
              name="dob"
              placeholder="1994"
              onChange={this.handleChange.bind(this)}
            />
          </Form.Field>
          <Form.Field>
            <label>Gender</label>
            <input
              type="text"
              name="gender"
              placeholder="Specify your gender"
              onChange={this.handleChange.bind(this)}
            />
          </Form.Field>
          <Button type="submit" positive>
            Save
          </Button>
          <Button type="submit" negative>
            Cancel
          </Button>
        </Form>
      </Container>
    );
  }
}

export default AddUser;
