import React from 'react';
import {
 Button,
 Form,
 Grid,
 Header,
 Message,
 Segment
} from 'semantic-ui-react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';

class LoginForm extends React.PureComponent {
 constructor(props) {
  super(props);
  this.state = {
   email: '',
   password: '',
   userAuthenticated: false
  };
 }

 login() {
  axios.post('https://reqres.in/api/login', this.state).then(res => {
   sessionStorage.setItem('token', res.data.token);
   this.setState({ userAuthenticated: true });
   setTimeout(() => {
    sessionStorage.removeItem('token');
    window.location.href = '/';
   }, 60000 * 3);
  });
 }

 handleChange(event) {
  this.setState({ [event.target.name]: event.target.value });
 }

 isAuthenticated() {
  const token = sessionStorage.getItem('token');
  if (token) {
   return true;
  } else {
   return false;
  }
 }
 render() {
  const isUserAuthenticated = this.isAuthenticated();
  return (
   <div>
    {isUserAuthenticated ? (
     <Redirect to={{ pathname: '/dashboard' }} />
    ) : (
     <div className="login-form">
      <Grid textAlign="center" style={{ height: '100%' }}>
       <Grid.Column style={{ maxWidth: 450 }}>
        <Header as="h2" color="teal" textAlign="center">
         Log-in to your account
        </Header>
        <Form size="large" onSubmit={this.login.bind(this)}>
         <Segment stacked>
          <Form.Input
           fluid
           name="email"
           icon="user"
           iconPosition="left"
           placeholder="E-mail address"
           onChange={this.handleChange.bind(this)}
          />
          <Form.Input
           fluid
           name="password"
           icon="lock"
           iconPosition="left"
           placeholder="Password"
           type="password"
           onChange={this.handleChange.bind(this)}
          />

          <Button type="submit" color="teal" fluid size="large">
           Login
          </Button>
         </Segment>
        </Form>
        <Message>
         New to us? <a href="/">Sign Up</a>
        </Message>
       </Grid.Column>
      </Grid>
     </div>
    )}
   </div>
  );
 }
}

export default LoginForm;
