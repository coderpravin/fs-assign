import React from 'react';
import { Table, Button } from 'semantic-ui-react';
import axios from 'axios';

class TableExampleStriped extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: []
    };
  }

  componentDidMount() {
    axios
      .get('https://5b2de73b47942a001493691b.mockapi.io/users')
      .then(response => {
        this.setState({ users: response.data });
      })
      .catch(error => {
        console.log(error);
      });
  }

  calculateAge(birthYear) {
    // let now = new Date();
    // let fetchedYear = moment(timestamp).format('YYYY');
    // let birthYear = fetchedYear - Math.floor(Math.random() * 50 + 16);
    // let currentYear = now.getFullYear();
    // return currentYear - birthYear;
    return 2018 - birthYear;
  }

  deleteUser(id) {
    if (window.confirm(`User with id ${id} will be removed, are you sure?`)) {
      this.setState({
        users: this.state.users.filter(user => user.id !== id)
      });
    }
  }

  render() {
    return (
      <div>
        <Table striped>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Avatar</Table.HeaderCell>
              <Table.HeaderCell>First Name</Table.HeaderCell>
              <Table.HeaderCell>Last Name</Table.HeaderCell>
              <Table.HeaderCell>Gender</Table.HeaderCell>
              <Table.HeaderCell>Age</Table.HeaderCell>
              <Table.HeaderCell>Email Address</Table.HeaderCell>
              <Table.HeaderCell>Mobile No.</Table.HeaderCell>
              <Table.HeaderCell>Action</Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {this.state.users.map((userDetail, index) => {
              return (
                <Table.Row key={index}>
                  <Table.Cell>
                    <img
                      src={userDetail.avatar}
                      width={30}
                      height={30}
                      style={{ borderRadius: '50%' }}
                      alt="img"
                    />
                  </Table.Cell>

                  <Table.Cell>{userDetail.firstName}</Table.Cell>
                  <Table.Cell>{userDetail.lastName}</Table.Cell>
                  <Table.Cell>{userDetail.gender}</Table.Cell>
                  <Table.Cell>{this.calculateAge(userDetail.dob)}</Table.Cell>
                  <Table.Cell>{userDetail.email}</Table.Cell>
                  <Table.Cell>{userDetail.mobile}</Table.Cell>
                  <Table.Cell>
                    <Button basic color="blue">
                      Edit
                    </Button>
                    <Button
                      onClick={this.deleteUser.bind(this, userDetail.id)}
                      basic
                      color="red"
                    >
                      Delete
                    </Button>
                  </Table.Cell>
                </Table.Row>
              );
            })}
          </Table.Body>
        </Table>
      </div>
    );
  }
}

export default TableExampleStriped;
