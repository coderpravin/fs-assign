import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import LoginForm from './components/Login/LoginForm';
import Dashboard from './components/Dashboard/Dashboard';
import AddUser from './components/AddUser/index';
import './App.css';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="container">
          <Route exact path="/" component={LoginForm} />
          <Route path="/dashboard" component={Dashboard} />
          <Route path="/adduser" component={AddUser} />
        </div>
      </Router>
    );
  }
}

export default App;
